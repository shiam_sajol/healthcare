//#include "app_control_h."
#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>
#include <app.h>
#include <app_alarm.h>
#include "MedicineReminderMainView.h"
#include "CommonUtil.h"
#include "telephony.h"

MedicineReminderMainView::MedicineReminderMainView() {

}
MedicineReminderMainView::~MedicineReminderMainView() {

}
Evas_Object* MedicineReminderMainView::create(Evas_Object* parent, Evas_Object* mainNaviframe) {

	naviframe = mainNaviframe;
	Evas_Object *box = elm_box_add(parent);
	elm_box_padding_set(box, 300, 300);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);

	datetime = elm_datetime_add(box);
	elm_object_content_set(box, datetime);
	evas_object_size_hint_align_set(datetime, EVAS_HINT_FILL, 0.5);
	evas_object_size_hint_weight_set(datetime, EVAS_HINT_EXPAND, 1.0);
	elm_object_style_set(datetime, "datepicker_layout");
	elm_win_resize_object_add(box, datetime);
	evas_object_show(datetime);
	elm_box_pack_end(box, datetime);

	elm_datetime_format_set(datetime, "%H : %M");
	//elm_datetime_show(dateti)
	//Evas_Object *setReminderButton;
	char buf[PATH_MAX];

	setReminderButton = elm_button_add(box);
	evas_object_size_hint_weight_set(setReminderButton, EVAS_HINT_EXPAND, 0.0);
	evas_object_size_hint_align_set(setReminderButton, EVAS_HINT_FILL,
	EVAS_HINT_FILL);
	snprintf(buf, sizeof(buf), "%s", "Set Reminder");
	elm_object_text_set(setReminderButton, buf);
	evas_object_smart_callback_add(setReminderButton, "clicked",
			setReminderButtonClickedCb, this);
	evas_object_show(setReminderButton);
	//elm_object_content_set(box, setReminderButton);
	evas_object_size_hint_min_set(setReminderButton, 300, 300);
	evas_object_size_hint_max_set(setReminderButton, 300, 300);
	elm_box_pack_end(box, setReminderButton);

	evas_object_show(box);

	eext_object_event_callback_add(box, EEXT_CALLBACK_BACK, layout_back_cb, this);

	return box;
}

void MedicineReminderMainView::layout_back_cb(void *data, Evas_Object *obj, void *event_info) {
	MedicineReminderMainView *self = (MedicineReminderMainView*) data;
	elm_naviframe_item_pop(self->naviframe);
}




void MedicineReminderMainView::setReminderButtonClickedCb(void *data,
		Evas_Object *obj, void *event_info) {
	MedicineReminderMainView *self =
			static_cast<MedicineReminderMainView*>(data);
	dlog_print(DLOG_DEBUG, LOG_TAG, "set reminder Button is clicked");
	struct tm t;
	elm_datetime_value_get(self->datetime, &t);
	dlog_print(DLOG_DEBUG, LOG_TAG, "time is Hour: %d", t.tm_hour);
	dlog_print(DLOG_DEBUG, LOG_TAG, "time is minute: %d", t.tm_min);

	int currtime = 6;
	int REMIND = 1;
	int alarm_id;
	app_control_h app_control = NULL;
	int ret = app_control_create(&app_control);
	ret = app_control_set_operation(app_control, APP_CONTROL_OPERATION_DEFAULT);
	ret = app_control_set_app_id(app_control, "org.tizen.medicine-alarm");
	int alarmId;
	int alarmCreated = alarm_schedule_at_date(app_control, &t, 0, &alarmId);
	dlog_print(DLOG_DEBUG, LOG_TAG, "alarm creation ret is %d", alarmCreated);
}

/*data->printTo;
 }
 void MedicineReminderMainView::printTo(){
 std::string resultToshowString = std::to_string(datetime);
 const char *resultToShow = resultToShowString.c_str();
 elm_object_text_set(datetime, resultToShow );
 }*/

