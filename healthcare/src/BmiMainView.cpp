/*
 * MainView.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: shejan
 */

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

#include "BmiMainView.h"
#include "CommonUtil.h"

BmiMainView::BmiMainView() {

}
BmiMainView::~BmiMainView() {

}

//global variable to hold bmi result
float xResult;
//global variable to hold bmi text field evas object
Evas_Object* bmiGField;
Evas_Object* gbmiRemarks;
// global var to hold the remarks
int remarks = 0;

// if 1 == under, 2 == normal , 3 == over wight, 4 == extremely obese

Evas_Object* BmiMainView::create(Evas_Object* parent, Evas_Object* mainNaviframe) {
	naviframe = mainNaviframe;
	dlog_print(DLOG_DEBUG, LOG_TAG, "created");
	char edj_path[PATH_MAX] = { 0, };
	app_get_resource(BMI_EDJ_FILE, edj_path, (int) PATH_MAX);
	layout = elm_layout_add(parent);
	elm_layout_file_set(layout, edj_path, BMI_GRP_MAIN);

	addHeightEntry();
	addWeightEntry();
	addCalculateButton();
	addBmiEntry();
	addBmiRemarks();

	eext_object_event_callback_add(layout, EEXT_CALLBACK_BACK, layout_back_cb,
			this);

	return layout;
}

void BmiMainView::addHeightEntry() {
	heightEntry = elm_entry_add(layout); // ekta entry banalam and layout er moddhe add korlam
	elm_entry_input_panel_layout_set(heightEntry,
			ELM_INPUT_PANEL_LAYOUT_NUMBER); //ei function e giye dekho onek rokom layout ase konta hobe check kore boshao
	elm_object_part_content_set(layout, "height_entry", heightEntry); //edc te ekta; part chilona height_entry name e sekhane ei entry ta boshiye dilam
	elm_object_text_set(heightEntry, "0");
	elm_entry_editable_set(heightEntry, true);
	evas_object_show(heightEntry);
}
void BmiMainView::addWeightEntry() {
	weightEntry = elm_entry_add(layout);
	elm_entry_input_panel_layout_set(weightEntry,
			ELM_INPUT_PANEL_LAYOUT_NUMBER);
	elm_object_part_content_set(layout, "weight_entry", weightEntry);
	elm_object_text_set(weightEntry, "0");
	elm_entry_editable_set(weightEntry, true);
	evas_object_show(weightEntry);
}
void BmiMainView::addBmiEntry() {
	bmiEntry = elm_entry_add(layout);
	elm_entry_input_panel_layout_set(bmiEntry, ELM_INPUT_PANEL_LAYOUT_NUMBER);
	elm_object_part_content_set(layout, "bmi_entry", bmiEntry);
	elm_object_text_set(bmiEntry, "0");
	elm_entry_editable_set(bmiEntry, false);
	evas_object_show(bmiEntry);
	bmiGField = bmiEntry;
}

void BmiMainView::addBmiRemarks() {
	bmiRemarks = elm_entry_add(layout);
	elm_entry_input_panel_layout_set(bmiRemarks, ELM_INPUT_PANEL_LAYOUT_NUMBER);
	elm_object_part_content_set(layout, "bmi_remarks", bmiRemarks);
	elm_object_text_set(bmiRemarks, "ENTER THE FIELD ABOVE!");
	elm_entry_editable_set(bmiRemarks, false);
	evas_object_show(bmiRemarks);
	gbmiRemarks = bmiRemarks;
}

void BmiMainView::addCalculateButton() {
	dlog_print(DLOG_DEBUG, "healthcares", "button create start");
	calculateButton = elm_button_add(layout);
	elm_object_part_content_set(layout, "button", calculateButton);
	evas_object_smart_callback_add(calculateButton, "clicked",
			onCalculateButtonClicked, this); //adding the callback if the button is clicked
	//elm_object_part_text_set(calculateButton, "calculate bmi");
	elm_object_text_set(calculateButton, "Click for BMI");
	evas_object_show(calculateButton);
	dlog_print(DLOG_DEBUG, "healthcares", "button create end");
}

void BmiMainView::printTo() {
	std::string resultToShowstring = std::to_string(xResult);
	const char *resultToShow = resultToShowstring.c_str();
	elm_object_text_set(bmiGField, resultToShow);
}

void BmiMainView::calculateAfterResult() {
	switch (remarks) {
	case 1:
		elm_object_text_set(gbmiRemarks, "You are under-weighted.");
		break;
	case 2:
		elm_object_text_set(gbmiRemarks, "You BMI is Normal!");
		break;
	case 3:
		elm_object_text_set(gbmiRemarks, "You are OVER-WEIGHTED.");
		break;
	case 4:
		elm_object_text_set(gbmiRemarks,
				"You are extremely obese. Do exercises and follow appropiate food chart");
		break;
	}

}

void BmiMainView::onCalculateButtonClicked(void *data, Evas_Object *obj,
		void *event_info) {
	dlog_print(DLOG_DEBUG, "healthcare", "buttonclicked");
// need to cast void data
	//MainView* self= void * ptr;
	BmiMainView *self = static_cast<BmiMainView *>(data);

	const char * text = elm_entry_entry_get(self->heightEntry);
	dlog_print(DLOG_DEBUG, "healthcare", "user height entry: %s", text);

	const char * text1 = elm_entry_entry_get(self->weightEntry);
	dlog_print(DLOG_DEBUG, "healthcare", "user weight entry: %s", text1);

	float xHeight = std::atof(text);
	float xWeight = std::atof(text1);

	xResult = (xHeight) * (xHeight);
	xResult = xWeight / xResult;

	//below 18.5 == under weight
	if (xResult <= 18.4) {
		remarks = 1;
	}

	// 18.5 - 24.9 == normal
	else if (xResult >= 18.5 && xResult < 24.9) {
		remarks = 2;
	}

	//25 - 29.9 == over weight
	else if (xResult >= 25 && xResult <= 29.8) {
		remarks = 3;
	}

	// 30> obese
	else if (xResult > 29.9) {
		// extremely obese
		remarks = 4;
	}
	self->printTo();
	self->calculateAfterResult();
}

void BmiMainView::layout_back_cb(void *data, Evas_Object *obj, void *event_info) {
	BmiMainView *self = (BmiMainView*) data;
	elm_naviframe_item_pop(self->naviframe);
}

