#include "NutritionMainView.h"
#include <Elementary.h>
#include <string>
using namespace std;
#include <dlog.h> // for logging purposes
#include <app_control.h>

typedef struct item_data {
	int index;
	Elm_Object_Item *item;
	Eina_Bool expanded;
} item_data_s;



/*
void NutritionMainView::ButtonClicked(void *data, Evas_Object *obj,
		void *event_info) {
	Doctor* doctor = static_cast<Doctor*>(data);
	dlog_print(DLOG_DEBUG, "Call2Doc", "%s is called", doctor->doctorName.c_str());

	dlog_print(DLOG_DEBUG, "Call2Doc", "called %s", doctor->doctorName.c_str());
	//char* chr = strdup(doctor->telephone.c_str());

	//call_number(chr);
}*/

NutritionMainView::NutritionMainView(Evas_Object *_naviframe) {
	naviframe = _naviframe;
}


Evas_Object* NutritionMainView::create(Evas_Object* parent, Evas_Object* mainNaviframe) {
	Evas_Object *list;
	//char buf[100] = { 0, };
	//const char *list_item[] = {"Call 2 Doc", "Ambulance Service"};
	naviframe = mainNaviframe;

	/* List */
	list = elm_list_add(parent);
	elm_list_mode_set(list, ELM_LIST_COMPRESS);
//	evas_object_smart_callback_add(list, "selected", list_selected_cb, NULL);

	/* Main Menu Items Here*/

//	elm_list_item_append(list, "Doctor", NULL, NULL, calldoc, nf);
//	elm_list_item_append(list, "Ambulance", NULL, NULL, ambulance, nf);
	elm_list_item_append(list, "Health Tips", NULL, NULL, NULL, NULL);
	elm_list_item_append(list, "Food ", NULL, NULL, NULL, NULL);
	elm_list_item_append(list, "Tips 1", NULL, NULL, NULL, NULL);
	elm_list_item_append(list, "Tips 1", NULL, NULL, NULL, NULL);
	elm_list_item_append(list, "Tips 1", NULL, NULL, NULL, NULL);


	elm_list_go(list);

	//eext_object_event_callback_add(list, EEXT_CALLBACK_BACK, layout_back_cb,
					//this);

	return list;

}
