/*
 * MainView.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: nafeez
 */

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>
#include <app_control.h>

#include "MainView.h"
#include "BmiMainView.h"
#include "CallDoctorMainView.h"
#include "AmbulanceMainView.h"
#include "CommonUtil.h"
#include "MedicineReminderMainView.h"
#include "MapMainView.h"
#include "NutritionMainView.h"

static const int imageMinHeight = 100;
#define ICON_DIR "/opt/usr/apps/org.tizen.healthcare/res/images"

class Item {
public:
	int index;
	char* imagePath;

    Item( int _index, char* _imagePath ) {
    	index = _index;
    	imagePath = _imagePath;
    }
};

static const Item items[] = { { 1, "bmi_calculator.jpg" }, { 2,
		"medicine_reminder.jpg" }, { 3, "call_doctor.jpg" }, { 4,
		"nearby_hospital.jpg" }, { 5, "nutrition_tips.jpg" }, { 6,
		"emergency_ambulance.jpg" } };

MainView::MainView() {

}
MainView::~MainView() {

}

void MainView::appCreated() {
	Evas_Object *bg = NULL;

	/* Window */
	win = elm_win_util_standard_add(PACKAGE, PACKAGE);
	elm_app_base_scale_set(1.8);

	elm_win_conformant_set(win, EINA_TRUE);
	elm_win_autodel_set(win, EINA_TRUE);

	if (elm_win_wm_rotation_supported_get(win)) {
		int rots[4] = { 0, 90, 180, 270 };
		elm_win_wm_rotation_available_rotations_set(win, (const int *) (&rots),
				4);
	}

	evas_object_smart_callback_add(win, "delete,request", win_delete_request_cb,
	NULL);

	/* Conformant */
	conform = elm_conformant_add(win);
	elm_win_indicator_mode_set(win, ELM_WIN_INDICATOR_SHOW);
	elm_win_indicator_opacity_set(win, ELM_WIN_INDICATOR_OPAQUE);
	evas_object_size_hint_weight_set(conform, EVAS_HINT_EXPAND,
	EVAS_HINT_EXPAND);
	elm_win_resize_object_add(win, conform);
	evas_object_show(conform);

	/* Indicator BG */
	bg = elm_bg_add(conform);
	elm_object_style_set(bg, "indicator/headerbg");
	elm_object_part_content_set(conform, "elm.swallow.indicator_bg", bg);
	evas_object_show(bg);

	/* Base Layout */
	layout = elm_layout_add(conform);
	evas_object_size_hint_weight_set(layout, EVAS_HINT_EXPAND,
	EVAS_HINT_EXPAND);
	elm_layout_theme_set(layout, "layout", "application", "default");
	evas_object_show(layout);

	elm_object_content_set(conform, layout);

	/* Naviframe */
	naviframe = elm_naviframe_add(layout);

	/* Main View */
	createMainListView();

	elm_naviframe_item_push(naviframe, "Health Care", NULL, NULL, scroller,
	NULL);

	elm_object_part_content_set(layout, "elm.swallow.content", naviframe);

	/* Show window after base gui is set up */
	evas_object_show(win);

	eext_object_event_callback_add(mainView, EEXT_CALLBACK_BACK, layout_back_cb,
			this);
}

void MainView::createMainListView() {
	Evas_Object *allItems;

	scroller = elm_scroller_add(win);
	elm_scroller_bounce_set(scroller, EINA_FALSE, EINA_TRUE);
	evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND,
	EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(scroller);

	mainView = elm_box_add(win);
	elm_box_align_set(mainView, 10, 0);
	evas_object_size_hint_weight_set(mainView, EVAS_HINT_EXPAND,
	EVAS_HINT_EXPAND);
	elm_box_padding_set(mainView, ELM_SCALE_SIZE(10), ELM_SCALE_SIZE(15));
	elm_object_content_set(scroller, mainView);

	allItems = createItemTable();
	evas_object_show(allItems);
	elm_box_pack_end(mainView, allItems);

}

Evas_Object* MainView::createItemTable() {
	Evas_Object *itemTable;

	itemTable = elm_table_add(mainView);
	elm_table_homogeneous_set(itemTable, EINA_TRUE);
	evas_object_size_hint_align_set(itemTable, EVAS_HINT_FILL,
	EVAS_HINT_FILL);
	elm_table_padding_set(itemTable, ELM_SCALE_SIZE(5), ELM_SCALE_SIZE(5));
	evas_object_show(itemTable);

	int row = 0;
	int column = 0;
	for (int i = 0; i < 6; i++) {
		Evas_Object *icon = createItemImage(items[i].imagePath);
		Evas_Object *content = createItemButton(itemTable, icon, i);
		evas_object_show(content);
		elm_table_pack(itemTable, content, column, row, 1, 1);
		column++;
		if (i % 2 != 0) {
			column = 0;
			row++;
		}
	}
	return itemTable;
}

Evas_Object* MainView::createItemImage(const char *imagePath) {
	char path[200] = { 0 };
	snprintf(path, sizeof(path), ICON_DIR"/%s", imagePath);
	Evas_Object *icon = elm_image_add(mainView);
	elm_image_file_set(icon, path, NULL);
	evas_object_size_hint_min_set(icon, 0, imageMinHeight);
	return icon;
}

Evas_Object* MainView::createItemButton(Evas_Object *parent,
		Evas_Object *content, int itemIndex) {
	Evas_Object *button = elm_button_add(parent);
	elm_object_style_set(button, "transparent");
	evas_object_size_hint_weight_set(button, EVAS_HINT_EXPAND, 0.0);
	evas_object_size_hint_align_set(button, EVAS_HINT_FILL, 0.0);
	evas_object_data_set(button, "index", (void*)itemIndex);
	elm_object_content_set(button, content);
	evas_object_smart_callback_add(button, "clicked", itemClickedCb, this);
	return button;
}

void MainView::itemClickedCb(void *user_data, Evas_Object *obj,
		void *event_info) {
	int index = (int)evas_object_data_get(obj, "index");
	dlog_print(DLOG_DEBUG, LOG_TAG, "item cliecked %d", index);
	Evas_Object *childView = nullptr;
	MainView *self = static_cast<MainView *> (user_data);
	switch(index) {
	case 0:
	{
		BmiMainView* bmiView = new BmiMainView();
		childView = bmiView->create(self->win, self->naviframe);
		elm_naviframe_item_push(self->naviframe, "BMI Calculator", NULL, NULL, childView, NULL);
		evas_object_show(childView);
		break;
	}
	case 1:
	{
		MedicineReminderMainView* mrView = new MedicineReminderMainView();
		childView = mrView->create(self->win, self->naviframe);
		elm_naviframe_item_push(self->naviframe, "Medicine Remainder", NULL, NULL, childView, NULL);
		evas_object_show(childView);
		break;
	}
	case 2:
	{
		CallDoctorMainView* calldocView = new CallDoctorMainView(self->naviframe);
		childView = calldocView->create(self->win, self->naviframe);
		elm_naviframe_item_push(self->naviframe, "24x7 Call Doctor", NULL, NULL, childView, NULL);
		evas_object_show(childView);
		break;
	}
	case 3:
	{
		MapMainView* mapView = new MapMainView();
		childView = mapView->create(self->win, self->naviframe);
		elm_naviframe_item_push(self->naviframe, "Search Hospitals", NULL, NULL, childView, NULL);
		evas_object_show(childView);
		break;
	}
	case 4:
	{
		NutritionMainView* nutView = new NutritionMainView(self->naviframe);
		childView = nutView->create(self->win, self->naviframe);
		elm_naviframe_item_push(self->naviframe, "Nutrition", NULL, NULL, childView, NULL);
		evas_object_show(childView);
		break;
	}
	case 5:
	{
		AmbulanceMainView* ambulanceView = new AmbulanceMainView(self->naviframe);
		childView = ambulanceView->create(self->win, self->naviframe);
		elm_naviframe_item_push(self->naviframe, "Emergency Ambulance", NULL, NULL, childView, NULL);
		evas_object_show(childView);
		break;
	}

	}
	//elm_naviframe_item_push(self->naviframe, "BMI Calculator", NULL, NULL, childView, NULL);
	//evas_object_show(childView);
//	elm_object_part_content_set(self->naviframe, childView);
}

void MainView::appResumed() {

}
void MainView::appTerminated() {

}
void MainView::appPaused() {

}
void MainView::appControlCalled() {

}

void MainView::win_delete_request_cb(void *data, Evas_Object *obj,
		void *event_info) {
	ui_app_exit();
}

void MainView::layout_back_cb(void *data, Evas_Object *obj, void *event_info) {
	MainView *mainView = (MainView*) data;
	/* Let window go to hide state. */
	elm_win_lower(mainView->win);
}
