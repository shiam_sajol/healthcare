/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *		  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include "CallDoctorMainView.h"
//#include "AmbulanceMainView.h"
#include <Elementary.h>
#include <string>
using namespace std;
#include <dlog.h> // for logging purposes
#include <app_control.h>

typedef struct item_data {
	int index;
	Elm_Object_Item *item;
	Eina_Bool expanded;
} item_data_s;

class Doctor {
public:
	int index;
	string doctorName;
	string telephone;
};

Doctor doctors[] = { { 0, "Dr. Iqbal", "0184294884" }, { 1, "Dr. Ahsan",
		"18839138" }, {2, "Dr. Kamal", "9983768"} };

CallDoctorMainView::CallDoctorMainView(Evas_Object *_naviframe) {
	naviframe = _naviframe;
}

void CallDoctorMainView::layout_back_cb(void *data, Evas_Object *obj, void *event_info) {
	CallDoctorMainView *self = (CallDoctorMainView*) data;
	elm_naviframe_item_pop(self->naviframe);
}

char* CallDoctorMainView::_text_get_cb(void *data, Evas_Object *obj, const char *part) {
		Doctor* doctor = static_cast<Doctor*>(data);
		if (strcmp(part, "elm.text") == 0) {
			return strdup(doctor->doctorName.c_str());
		} else {
			return NULL;
		}

}

//char* CallDoctorMainView::_text_get2_cb(void *data, Evas_Object *obj, const char *part)
//{
//	//item_data_s *id = data;
//	// Check this is text for the expected part
//	   if (strcmp(part, "elm.text") == 0)
//	   {
//	      return strdup("Dr. Ahsan");
//	   }
//	   else
//	   {
//	      return NULL;
//	   }
//}
//
//char* CallDoctorMainView::_text_get3_cb(void *data, Evas_Object *obj, const char *part)
//{
//	//item_data_s *id = data;
//	// Check this is text for the expected part
//	   if (strcmp(part, "elm.text") == 0)
//	   {
//	      return strdup("Dr. Kamal");
//	   }
//	   else
//	   {
//	      return NULL;
//	   }
//}
/*
 Evas_Object* CallDoctorMainView::_item_content_get(void *data, Evas_Object *obj, const char *part)
 {
 int i = (int) (uintptr_t) data;

 if (strcmp(part, "contacts_ic_circle_btn_call.png") == 0) {
 Evas_Object *bg = elm_bg_add(obj);
 elm_bg_color_set(bg, 255 * cos(i / (double) 10), 0, i % 255);
 return bg;
 }
 else if (strcmp(part, "elm.swallow.end") == 0) {
 Evas_Object *bg = elm_bg_add(obj);
 elm_bg_color_set(bg, 0, 255 * sin(i / (double) 10), i % 255);
 return bg;
 }
 else {
 return NULL;
 }
 }*/

void CallDoctorMainView::gl_del_cb(void *data, Evas_Object *obj) {
	/* Unrealized callback can be called after this. */
	/* Accessing item_data_s can be dangerous on unrealized callback. */
//	item_data_s *id = (item_data_s*) data;
}

void CallDoctorMainView::call_number(char* number) {
	app_control_h app_control = NULL;
	app_control_create(&app_control);
	app_control_set_app_id(app_control, "com.samsung.call");
	app_control_add_extra_data(app_control, "number", number);
	app_control_add_extra_data(app_control, "launch-type", "MO");
	if(app_control_send_launch_request(app_control, NULL, NULL) == APP_CONTROL_ERROR_NONE)
	   //LOGI("Phone launched!");

	app_control_destroy(app_control);
}

void CallDoctorMainView::ButtonClicked(void *data, Evas_Object *obj,
		void *event_info) {
	Doctor* doctor = static_cast<Doctor*>(data);
	dlog_print(DLOG_DEBUG, "Call2Doc", "%s is called", doctor->doctorName.c_str());

	dlog_print(DLOG_DEBUG, "Call2Doc", "called %s", doctor->doctorName.c_str());
	char* chr = strdup(doctor->telephone.c_str());

	call_number(chr);
}

void CallDoctorMainView::genlist_test_cb(void *data, Evas_Object *obj,
		void *event_info) {
	CallDoctorMainView* self = static_cast<CallDoctorMainView*>(data);

	Evas_Object *genlist;
	Elm_Object_Item *it = (Elm_Object_Item*) event_info;
	Elm_Genlist_Item_Class *itc = NULL;

	const char *style = elm_object_item_text_get(it);

	printf("Item style: %s\n", style);
	elm_list_item_selected_set(it, EINA_FALSE);

	genlist = elm_genlist_add(self->naviframe);
	elm_genlist_mode_set(genlist, ELM_LIST_EXPAND);


	printf("Compress mode enabled\n");
	for (int i = 0; i < 3; i++) {
		itc = elm_genlist_item_class_new();
		itc->item_style = "default";
		itc->func.content_get = NULL;
		itc->func.text_get = _text_get_cb;
		itc->func.del = gl_del_cb;
		elm_genlist_item_append(genlist, // genlist object
				itc,  // item class (append group_index item per 7 items)
				(void*) &doctors[i],   // item class user data
				NULL, // parent
				ELM_GENLIST_ITEM_NONE, // item type
				ButtonClicked, // select smart callback
				(void*) &doctors[i]);  // smart callback user data
		elm_genlist_item_class_free(itc);

		eext_object_event_callback_add(genlist, EEXT_CALLBACK_BACK, layout_back_cb, NULL);
	}

	/* Create item class */
//
//	itc = elm_genlist_item_class_new();
//	itc->item_style = "default";
//	itc->func.content_get = NULL;
//	itc->func.text_get = _text_get_cb;
//	itc->func.del = gl_del_cb;
//
//	itc3 = elm_genlist_item_class_new();
//	itc3->item_style = "default";
//	itc3->func.content_get = NULL;
//	itc3->func.text_get = _text_get3_cb;
//	itc3->func.del = gl_del_cb;
//
//	itc2 = elm_genlist_item_class_new();
//	itc2->item_style = "default";
//	itc2->func.content_get = NULL;
//	itc2->func.text_get = _text_get2_cb;
//	itc2->func.del = gl_del_cb;
//
//	genlist = elm_genlist_add(self->naviframe);
//
//	//elm_genlist_block_count_set(genlist, 3);
//
//	elm_genlist_mode_set(genlist, ELM_LIST_EXPAND);
//	printf("Compress mode enabled\n");
//
//	if (strcmp("Call 2 Doc", style))
//		elm_genlist_homogeneous_set(genlist, EINA_TRUE);
//
//	item_data_s *id = (item_data_s*) calloc(sizeof(item_data_s), 1);
//
//	elm_genlist_item_append(genlist, // genlist object
//			itc,  // item class (append group_index item per 7 items)
//			NULL,   // item class user data
//			NULL, // parent
//			ELM_GENLIST_ITEM_NONE, // item type
//			ButtonClicked, // select smart callback
//			id);  // smart callback user data
//
//	elm_genlist_item_append(genlist, // genlist object
//			itc3,  // item class (append group_index item per 7 items)
//			NULL,   // item class user data
//			NULL, // parent
//			ELM_GENLIST_ITEM_NONE, // item type
//			NULL, // select smart callback
//			id);  // smart callback user data
//
//	elm_genlist_item_append(genlist, // genlist object
//			itc2,  // item class
//			NULL,   // item class user data
//			NULL, ELM_GENLIST_ITEM_TREE, // item type
//			NULL, // select smart callback
//			id);  // smart callback user data
//	elm_genlist_item_class_free(itc);
//	if (itc2)
//		elm_genlist_item_class_free (itc2);
	elm_naviframe_item_push(self->naviframe, style, NULL, NULL, genlist, NULL);
}

Evas_Object* CallDoctorMainView::create(Evas_Object* parent, Evas_Object* mainNaviframe) {
	Evas_Object *list;
	//char buf[100] = { 0, };
	//const char *list_item[] = {"Call 2 Doc", "Ambulance Service"};
	naviframe = mainNaviframe;

	/* List */
	list = elm_list_add(parent);
	elm_list_mode_set(list, ELM_LIST_COMPRESS);
//	evas_object_smart_callback_add(list, "selected", list_selected_cb, NULL);

	/* Main Menu Items Here*/

//	elm_list_item_append(list, "Doctor", NULL, NULL, calldoc, nf);
//	elm_list_item_append(list, "Ambulance", NULL, NULL, ambulance, nf);
	elm_list_item_append(list, "Call 2 Doc", NULL, NULL, genlist_test_cb, this);
	//elm_list_item_append(list, "Ambulance Service", NULL, NULL, , this);

	elm_list_go(list);

	eext_object_event_callback_add(list, EEXT_CALLBACK_BACK, layout_back_cb,
					this);

	return list;

}
