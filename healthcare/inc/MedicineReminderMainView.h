/*
 * MainView.h
 *
 *  Created on: Jun 1, 2016
 *      Author: nafeez
 */

#ifndef MEDICINE_REMINDER_MAINVIEW_H_
#define MEDICINE_REMINDER_MAINVIEW_H_

#include "AppTemplate.h"

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

class MedicineReminderMainView  {
public:
	MedicineReminderMainView();
	~MedicineReminderMainView();

public:
	Evas_Object* create(Evas_Object* parent, Evas_Object* mainNaviframe);
private:
	static void setReminderButtonClickedCb(void *data, Evas_Object *obj, void *event_info);
	static void layout_back_cb(void *data, Evas_Object *obj, void *event_info);
public:
	Evas_Object* datetime = nullptr;
	Evas_Object* setReminderButton = nullptr;

private:
	Evas_Object* naviframe = nullptr;
};




#endif /* MEDICINE_REMINDER_MAINVIEW_H_ */
