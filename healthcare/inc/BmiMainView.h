/*
 * MainView.h
 *
 *  Created on: Jun 1, 2016
 *      Author: shejan
 */

#ifndef BMI_MAINVIEW_H_
#define BMI_MAINVIEW_H_

#include "AppTemplate.h"

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <sstream>


using namespace std;

class BmiMainView {
public:
	BmiMainView();
	~BmiMainView();

public:
	Evas_Object* create(Evas_Object* parent, Evas_Object* mainNaviframe);

private:
	void addHeightEntry();
	void addWeightEntry();
	void addCalculateButton();
	void addBmiEntry();
	void addBmiRemarks();
	void printTo();
	void calculateAfterResult();
	static void onCalculateButtonClicked(void *data, Evas_Object *obj, void *event_info);
	static void win_delete_request_cb(void *data, Evas_Object *obj, void *event_info);
	static void layout_back_cb(void *data, Evas_Object *obj, void *event_info);


public:
	Evas_Object* layout = nullptr;
	Evas_Object* naviframe = nullptr;

	Evas_Object* heightEntry = nullptr;
	Evas_Object* weightEntry = nullptr;
	Evas_Object* calculateButton = nullptr;
	Evas_Object* bmiEntry = nullptr;
	Evas_Object* bmiRemarks = nullptr;
};




#endif /* MAINVIEW_H_ */
