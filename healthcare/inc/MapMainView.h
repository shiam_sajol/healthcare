/*
 * MainView.h
 *
 *  Created on: Jun 1, 2016
 *      Author: shejan
 */

#ifndef MAP_MAINVIEW_H_
#define MAP_MAINVIEW_H_

#include "AppTemplate.h"

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>
#include <stdio.h>
#include <stdlib.h>

typedef enum
{
	MAPS_VIEW_MODE_MY_LOCATION,
	MAPS_VIEW_MODE_POI_INFO,
	MAPS_VIEW_MODE_DIRECTION
} MAP_VIEW_MODE;

class MapMainView {
public:
	MapMainView();
	~MapMainView();

public:
	Evas_Object* create(Evas_Object* parent, Evas_Object* mainNaviframe);

private:
	void init();
	void loadMap();
	Evas_Object* createMapObject();
	static void layout_back_cb(void *data, Evas_Object *obj, void *event_info);
public:
	Evas_Object* naviframe = nullptr;
	Evas_Object* layout = nullptr;
	Evas_Object *m_map_obj_layout = NULL;
	Evas_Object *m_map_evas_object = NULL;
	Evas_Object *m_searchbar_obj = NULL;
	Evas_Object *m_map_view_layout = NULL;
	Evas_Object *m_parent_evas_obj = NULL;

	MAP_VIEW_MODE __view_type = MAPS_VIEW_MODE_MY_LOCATION;
	bool __is_long_pressed = false;


	Elm_Map_Overlay *__m_poi_overlay[50];
	int __m_poi_overlay_count;
	Elm_Map_Overlay *__m_poi_current_overlay = NULL;
	Elm_Map_Overlay *__m_maneuver_overlay[1000];
	int __m_maneuver_overlay_count;
	int __m_route_overlay_count;
	Elm_Map_Overlay *__m_start_overlay = NULL;
	Elm_Map_Overlay *__m_dest_overlay = NULL;
	Elm_Map_Overlay *__m_route_overlay[1000];

};




#endif /* MAINVIEW_H_ */
