#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <dlog.h>
#include <efl_extension.h>

#include "AppTemplate.h"

#define TIZEN_ENGINEER_MODE
#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "uicomponents"

#define ELM_DEMO_EDJ "/opt/usr/apps/org.example.uicomponents/res/uicomponents.edj"
#define ICON_DIR "/opt/usr/apps/org.example.uicomponents/res/images"


class NutritionMainView {

public:
	NutritionMainView(Evas_Object *naviframe);
	Evas_Object* create(Evas_Object* parent, Evas_Object* mainNaviframe);

private:


	//static void genlist_cb(void *data, Evas_Object *obj, void *event_info);
	static void genlist_test_cb(void *data, Evas_Object *obj, void *event_info);
	//static void gl_del_cb(void *data, Evas_Object *obj);
	static void ButtonClicked(void *data, Evas_Object *obj, void *event_info);
	//static void call_number(char* number);
	//Evas_Object* _item_content_get(void *data, Evas_Object *obj, const char *part);
	//static char* _text_get_cb(void *data, Evas_Object *obj, const char *part);
	//static void layout_back_cb(void *data, Evas_Object *obj, void *event_info);

private:
	Evas_Object* naviframe = nullptr;
};
