#ifndef _COMMON_UTIL_H
#define _COMMON_UTIL_H

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "healthcare"

#if !defined(PACKAGE)
#define PACKAGE "org.tizen.healthcare"
#endif

#define EDJ_FILE "edje/healthcare.edj"
#define BMI_EDJ_FILE "edje/bmi_view.edj"
#define MAP_VIEW_EDJ_FILE "edje/mapview.edj"

#define GRP_MAIN "main"
#define BMI_GRP_MAIN "bmi_main"

static void
app_get_resource(const char *edj_file_in, char *edj_path_out, int edj_path_max)
{
	char *res_path = app_get_resource_path();
	if (res_path) {
		snprintf(edj_path_out, edj_path_max, "%s%s", res_path, edj_file_in);
		free(res_path);
	}
}

#endif /* _COMMON_UTIL_H */
