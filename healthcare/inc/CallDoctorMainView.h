/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd All Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <dlog.h>
#include <efl_extension.h>

#include "AppTemplate.h"

#define TIZEN_ENGINEER_MODE
#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "uicomponents"

#if !defined(PACKAGE)
#define PACKAGE "org.example.uicomponents"
#endif

#define ELM_DEMO_EDJ "/opt/usr/apps/org.example.uicomponents/res/uicomponents.edj"
#define ICON_DIR "/opt/usr/apps/org.example.uicomponents/res/images"




class CallDoctorMainView {

public:
	CallDoctorMainView(Evas_Object *naviframe);
	Evas_Object* create(Evas_Object* parent, Evas_Object* mainNaviframe);

private:


	//static void genlist_cb(void *data, Evas_Object *obj, void *event_info);
	static void genlist_test_cb(void *data, Evas_Object *obj, void *event_info);
	static void gl_del_cb(void *data, Evas_Object *obj);
	static void ButtonClicked(void *data, Evas_Object *obj, void *event_info);
	static void call_number(char* number);
	//Evas_Object* _item_content_get(void *data, Evas_Object *obj, const char *part);
	static char* _text_get_cb(void *data, Evas_Object *obj, const char *part);
	static void layout_back_cb(void *data, Evas_Object *obj, void *event_info);

private:
	Evas_Object* naviframe = nullptr;
};
