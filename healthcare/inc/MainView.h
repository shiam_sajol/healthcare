/*
 * MainView.h
 *
 *  Created on: Jun 1, 2016
 *      Author: nafeez
 */

#ifndef MAINVIEW_H_
#define MAINVIEW_H_

#include "AppTemplate.h"

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>


class MainView : public AppTemplate {
public:
	MainView();
	~MainView();
public:
	virtual void appCreated();
	virtual void appResumed();
	virtual void appTerminated();
	virtual void appPaused();
	virtual void appControlCalled();

private:
	void createMainListView();
	Evas_Object* createItemTable();
	Evas_Object* createItemImage(const char *imagePath);
	Evas_Object* createItemButton(Evas_Object *parent, Evas_Object *content, int itemIndex);

	static void itemClickedCb(void *user_data, Evas_Object *obj, void *event_info);
	static void win_delete_request_cb(void *data, Evas_Object *obj, void *event_info);
	static void layout_back_cb(void *data, Evas_Object *obj, void *event_info);


public:
	Evas_Object* win = nullptr;
	Evas_Object* conform = nullptr;
	Evas_Object* naviframe = nullptr;
	Evas_Object* scroller = nullptr;
	Evas_Object* mainView = nullptr;
	Evas_Object* layout = nullptr;
	int selectedIndex = 0;
};




#endif /* MAINVIEW_H_ */
