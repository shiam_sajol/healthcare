#ifndef __medicinealarmapp_H__
#define __medicinealarmapp_H__

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "medicinealarmapp"

#if !defined(PACKAGE)
#define PACKAGE "org.tizen.medicine-alarm"
#endif

#define EDJ_FILE "edje/medicinealarmapp.edj"
#define GRP_MAIN "main"


#endif /* __medicinealarmapp_H__ */
